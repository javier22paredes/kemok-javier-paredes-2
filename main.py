"""
	Problema 1
	En Kemok se está enseñando a bailar un robot, sin embargo, esté sólo tiene movimientos hacia adelante y hacia atrás.
	Los pasos son los siguientes:
		● Inicialmente el robot se encuentra en la posición 0 (cero).
		● En el primer paso ( P ), el robot se mueve 1 pasos, es decir, un paso hacia adelante, 1
		  quedando en la posición 1.
		● En el segundo paso ( P ), el robot se mueve -3 pasos, es decir, tres pasos hacia atrás 2
		  (note que el signo negativo indica movimientos hacia atrás), quedando en la posición -2.
		● Para los siguientes ( Pk), el robot se desplaza el resultado de la resta del último paso
		  dado y el penúltimo ( P ). Por ejemplo, = (-3) - (1) = -4 y = (-4) - (-3) = -1.
	Se requiere que programe un método que permita determinar qué movimiento hará el robot en
	el paso k ( P ) y la posición que tendrá luego de éste movimiento respecto a la posición inicial. k
	Es importante que considere el tiempo de ejecución. Pues para cada paso que debe dar el
	robot, se llamará a éste método y se espera que el baile sea fluido, es decir, que no demore
	demasiado tiempo determinando cuál será el siguiente paso a realizar. Como ayuda, piense en
	qué pasaría si se desea que el robot inicie su baile en el paso 2020 ( P ).
"""

"""
	Problema 1
	En Kemok se está enseñando a bailar un robot, sin embargo, esté sólo tiene movimientos hacia adelante y hacia atrás.
	Los pasos son los siguientes:
		● Inicialmente el robot se encuentra en la posición 0 (cero).
		● En el primer paso ( P ), el robot se mueve 1 pasos, es decir, un paso hacia adelante, 1
		  quedando en la posición 1.
		● En el segundo paso ( P ), el robot se mueve -3 pasos, es decir, tres pasos hacia atrás 2
		  (note que el signo negativo indica movimientos hacia atrás), quedando en la posición -2.
		● Para los siguientes ( Pk), el robot se desplaza el resultado de la resta del último paso
		  dado y el penúltimo ( P ). Por ejemplo, = (-3) - (1) = -4 y = (-4) - (-3) = -1.
	Se requiere que programe un método que permita determinar qué movimiento hará el robot en
	el paso k ( P ) y la posición que tendrá luego de éste movimiento respecto a la posición inicial. k
	Es importante que considere el tiempo de ejecución. Pues para cada paso que debe dar el
	robot, se llamará a éste método y se espera que el baile sea fluido, es decir, que no demore
	demasiado tiempo determinando cuál será el siguiente paso a realizar. Como ayuda, piense en
	qué pasaría si se desea que el robot inicie su baile en el paso 2020 ( P ).
"""

def baileRobot(cantidadPasos):
	cont= 0
	paso, anterior_paso= 0,0 #paso es el ultimo paso vez y anterior paso es el penultimo paso hay es donde se calcula los P(k)
	dance = 0 # la dance son los pasos de adelante y hacia atras que da el robot si es positivo va hacia adelante y si es negativo va hacia atras
	posicion = 0 # Es la ultima posicion donde se queda el robot al final del baile se inicializa en la posición 0
	while cantidadPasos>=cont:
		if cont == 1:
			paso = 1
			dance = paso
			posicion = paso
		elif cont == 2:
			anterior_paso = 1
			paso = -3
			posicion = paso+anterior_paso
			dance = paso+anterior_paso
		else:
			dance = paso-anterior_paso
			paso,anterior_paso = dance,paso
			posicion+=dance
		cont+=1
	return posicion,dance

n = int(input("Insertar la cantidad de pasos del Robot: "))
print("El robot esta en la posición: "+str(baileRobot(n)[0])+" y se movio una cantida de pasos de "+str(baileRobot(n)[1]))