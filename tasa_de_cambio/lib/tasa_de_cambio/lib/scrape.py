import urllib.request
from datetime import date
from datetime import datetime
from bs4 import BeautifulSoup


inicio = ""
fin = ""
tipo_busqueda = 0
valores = ""

class Scrape(object):

	def __init__(self, f_inicio):
		self.valores = ""
		self.inicio = f_inicio
		self.fin = date.today()
		if (self.inicio == self.fin):
			self.tipo_busqueda = 0
		else:
			self.tipo_busqueda = 1



	def consultar_tabla_url(self):
		d_ini = str(self.inicio.day)
		m_ini = str(self.inicio.month)
		y_ini = str(self.inicio.year)
		d_fin = str(self.fin.day)
		m_fin = str(self.fin.month)
		y_fin = str(self.fin.year)
		url = 'http://banguat.gob.gt/cambio/historico.asp'
		url += '?kmoneda=02&ktipo=5'
		url += '&kdia=' + d_ini + '&kmes=' + m_ini + '&kanio=' + y_ini
		url += '&kdia1=' + d_fin + '&kmes1=' + m_fin + '&kanio1=' + y_fin
		url += '&kcsv=ON&submit1=Consultar'
		# print(url)
		try:
			respuesta = urllib.request.urlopen(url)
		except:
			print("No se logro la conexión")
			return("")
		
		soup = BeautifulSoup(respuesta,'html.parser')
		return(soup)


	def generar_valores(self):
		pos_ini = 0
		cad_html = str(self.consultar_tabla_url())
		reg = ""
		reg_val = 0
		while (pos_ini >= 0):
			pos_ini = cad_html.find('<td align="center" class="mitexto">',pos_ini + 1)
			pos_fin = cad_html.find("tr>",pos_ini)
			cad = cad_html[pos_ini:pos_fin]
			
			ini = cad.find('>',0)
			fin = cad.find("<",ini + 1)
			dia = cad[ini+1:fin].strip()
			if (len(dia) < 6):
				break
			dia_f = datetime.strptime(dia, '%d/%m/%Y')
			dia = str(dia_f)

			ini = cad.find('>',fin + 10)
			fin = cad.find("<",ini + 1)
			compra = cad[ini+1:fin].strip()

			ini = cad.find('>',fin + 10)
			fin = cad.find("<",ini + 1)
			venta = cad[ini+1:fin].strip()
			
			if (len(dia) >= 6):
				reg = "('" + dia + "',"
				reg += compra + "," + venta + ")"
			else:
				break
			if (reg_val == 0):
				self.valores = reg
				reg_val = reg_val + 1
			else:
				self.valores += "," + reg
				reg_val = reg_val + 1
